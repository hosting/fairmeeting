
export default {
    /**
     * The URL with extra information about the app / service.
     */
    aboutURL: 'https://fairkom.eu/fairmeeting',

    /**
     * The URL to the source code repository.
     */
    sourceURL: 'https://git.fairkom.net/hosting/fairmeeting',

    /**
     * Application name.
     */
    appName: 'fairmeeting',

    /**
    * The prefix for application protocol.
    * You will also need to replace this in package.json.
    */
    appProtocolPrefix: 'jitsi-meet',

    /**
     * The default server URL of Jitsi Meet Deployment that will be used.
     */
    defaultServerURL: 'https://fairmeeting.net',

    /**
     * The default server Timeout in seconds.
     */
    defaultServerTimeout: 30,

    /**
     * URL to send feedback.
     */
    feedbackURL: 'https://git.fairkom.net/hosting/fairmeeting/-/issues',

    /**
     * The URL of Privacy Policy Page.
     */
    privacyPolicyURL: 'https://www.fairkom.eu/privacy',

    /**
     * The URL of Terms and Conditions Page.
     */
    termsAndConditionsURL: 'https://www.fairkom.eu/imprint'
};
