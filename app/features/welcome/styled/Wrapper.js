// @flow

import styled from 'styled-components';

export default styled.div`
    background: #f2f2f2;
    display: flex;
    flex-direction: column;
    height: 100vh;
`;
