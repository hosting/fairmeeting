// @flow

import styled from 'styled-components';

export default styled.span`
    color: #82ad29;
    font-weight: bold;
`;
